import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';

const ListItem = ({ note, onPress }) => {
  const { title, done = false } = note;
  const [isDone, setIsDone] = useState(done);

  const handlePress = () => {
    setIsDone((done) => !done);
    onPress(note);
  };

  return (
    <TouchableOpacity
      style={
        isDone ? { ...styles.container, ...styles.done } : styles.container
      }
      onPress={handlePress}
    >
      <Text
        style={isDone ? { ...styles.text, ...styles.textDone } : styles.text}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
    backgroundColor: '#fafa1e',
    flex: 1,
    marginVertical: 3,
  },
  done: {
    backgroundColor: '#dedede',
  },
  text: {
    fontSize: 15,
    color: '#000',
    textAlign: 'center',
  },
  textDone: {
    textDecorationLine: 'line-through',
  },
});

export default ListItem;
