import React, { useState } from 'react';
import { FlatList, StyleSheet, TextInput, View } from 'react-native';
import ListItem from '../components/ListItem';

const HomeScreen = () => {
  const [toDoItems, setToDoItems] = useState([]);
  const [doneItems, setDoneItems] = useState([]);

  const [text, setText] = useState('');

  const handleSubmit = () => {
    setToDoItems(() => [...toDoItems, { title: text, done: false }]);
    setText(() => '');
  };

  const handlePress = (task) => {
    const isDone = doneItems.indexOf(task) > -1;

    const itemFinder = (item) => item.title == task.title;
    const item = !isDone
      ? toDoItems.find(itemFinder)
      : doneItems.find(itemFinder);

    if (isDone) {
      const newItems = [...doneItems];
      newItems.splice(newItems.indexOf(item), 1);

      setDoneItems(() => [...newItems]);
      setToDoItems(() => [item, ...toDoItems]);
    } else {
      const newItems = [...toDoItems];
      newItems.splice(newItems.indexOf(item), 1);

      setToDoItems(() => [...newItems]);
      setDoneItems(() => [item, ...doneItems]);
    }
  };

  return (
    <View style={styles.container}>
      <View>
        <TextInput
          style={styles.form}
          placeholder="Insira um texto e pressione Enviar"
          value={text}
          onChangeText={(t) => setText(() => t)}
          onSubmitEditing={handleSubmit}
        />
      </View>
      <FlatList
        data={[...toDoItems, ...doneItems]}
        renderItem={(item) => {
          return <ListItem note={item.item} onPress={handlePress} />;
        }}
        keyExtractor={(item) => item.title}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'stretch',
    marginTop: 50,
    justifyContent: 'center',
  },
  form: {
    borderColor: 'black',
    borderBottomWidth: 2,
    padding: 10,
    margin: 20,
  },
});

export default HomeScreen;
